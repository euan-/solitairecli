package com.euan.solitaire.play

import com.euan.solitaire.ai.SolitairePlayer
import com.euan.solitaire.displayer.Displayer
import com.euan.solitaire.model._
import org.scalatest.FreeSpec

class InputParserTest extends FreeSpec {

  "should" - {

    val setup = new Setup
    val displayer = new Displayer()
    val game = Game(setup.deck, setup.bases, setup.lanes)
    val solitairePlayer = SolitairePlayer(game, displayer)
    val inputParser: InputParser = new InputParser

    val emptyLanes: List[Lane] = {
      List(
        Lane(1, List(), List()),
        Lane(2, List(), List()),
        Lane(3, List(), List()),
        Lane(4, List(), List()),
        Lane(5, List(), List()),
        Lane(6, List(), List()),
        Lane(7, List(), List()),
      )
    }

    val aceCard = Card(Rank.Ace, Suite.Heart)
    val twoCard = Card(Rank.Two, Suite.Spade)
    val threeCard = Card(Rank.Three, Suite.Heart)

    "process a move, specifically" - {

      "deck to base" in {
        val game = Game(List(aceCard), setup.bases, setup.lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.deckToBaseExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(game.deck.isEmpty)
        assert(game.bases.filter(_.suite == aceCard.suite).head.lastVisibleCard().get == aceCard)
        assert(moveMade == "Moved  A♥ to base")
      }

      "deck to lane" in {
        val lane1 = Lane(1, List(), List(twoCard))
        val lanes = List(lane1) ++ emptyLanes.drop(1)

        val game = Game(List(aceCard), setup.bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.deckToLaneOneExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(game.deck.isEmpty)
        assert(game.lanes.filter(_.place == 1).head.lastVisibleCard().get == aceCard)
        assert(moveMade == "Moved  A♥ to lane 1")
      }

      "base to lane" in {
        val lane1 = Lane(1, List(), List(twoCard))
        val lanes = List(lane1) ++ emptyLanes.drop(1)

        val heartBase = Base(Suite.Heart, List(aceCard))
        val bases = setup.bases.filterNot(_.suite == Suite.Heart) ++ List(heartBase)

        val game = Game(List(), bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.heartBaseToLaneOneExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(game.deck.isEmpty)
        assert(game.lanes.filter(_.place == 1).head.lastVisibleCard().get == aceCard)
        assert(moveMade == "Moved  A♥ to lane 1")
      }

      "lane to lane" in {
        val lane1 = Lane(1, List(), List(threeCard, twoCard))
        val lane2 = Lane(2, List(), List(aceCard))
        val lanes = List(lane1, lane2) ++ emptyLanes.drop(2)

        val game = Game(List(), setup.bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.laneTwoOneCardDownToLaneOneExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(game.deck.isEmpty)
        assert(game.lanes.filter(_.place == 1).head.lastVisibleCard().get == aceCard)
        assert(game.lanes.filter(_.place == 2).head.visibleCards.isEmpty)
        assert(moveMade == "Moved  A♥ to lane 1")
      }

      "lane to base" in {
        val lane1 = Lane(1, List(), List(threeCard, twoCard))
        val lanes = List(lane1) ++ emptyLanes.drop(1)

        val spadeBase = Base(Suite.Spade, List(Card(Rank.Ace, Suite.Spade)))
        val bases = setup.bases.filterNot(_.suite == Suite.Spade) ++ List(spadeBase)

        val game = Game(List(), bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.laneOneSecondCardDownToBaseExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(game.deck.isEmpty)
        assert(game.bases.filter(_.suite == Suite.Spade).head.lastVisibleCard().get == twoCard)
        assert(game.lanes.filter(_.place == 1).head.lastVisibleCard().get == threeCard)
        assert(moveMade == "Moved  2♠ to base")
      }

      "draw a card" in {
        val game = Game(List(aceCard, twoCard), setup.bases, setup.lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.drawACardExample.replaceAll("\"", "")

        assert(game.deck.head == aceCard)

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(game.deck.nonEmpty)
        assert(game.deck.size == 2)
        assert(game.deck.head == twoCard)
        assert(moveMade == "Drawing a Card")
      }

      "list available moves" in {
        val game = Game(setup.deck, setup.bases, setup.lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.availableMovesExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(moveMade != null)
      }

      "ask for help" in {
        val game = Game(setup.deck, setup.bases, setup.lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.askForHelpExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(moveMade != null)
      }

      "end game" in {
        val game = Game(setup.deck, setup.bases, setup.lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        val input = Constants.endGameExample.replaceAll("\"", "")

        val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)

        assert(moveMade == "Ending Game - Thanks for playing!")
      }

    }

  }

}
