package com.euan.solitaire.ai

import com.euan.solitaire.displayer.Displayer
import com.euan.solitaire.model._
import org.scalatest.FreeSpec

class SolitairePlayerTest extends FreeSpec {

  "should" - {

    val setup = new Setup
    val displayer = new Displayer()

    val card1 = Card(Rank.Two, Suite.Heart)
    val card2 = Card(Rank.Three, Suite.Spade)

    val emptyLanes: List[Lane] = {
      List(
        Lane(1, List(), List()),
        Lane(2, List(), List()),
        Lane(3, List(), List()),
        Lane(4, List(), List()),
        Lane(5, List(), List()),
        Lane(6, List(), List()),
        Lane(7, List(), List()),
      )
    }

    "carry out a move, specifically" - {

      "first in lane to lane revealing hidden card" in {
        val lane1 = Lane(1, List(Card(Rank.Six, Suite.Heart)), List(card2, card1))
        val lane2 = Lane(2, List(), List(Card(Rank.Four, Suite.Heart)))

        val lanes = List(lane1, lane2) ++ emptyLanes.drop(2)

        val game = Game(List(), setup.bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        solitairePlayer.play(100)

        assert(game.deck.isEmpty)
        assert(game.lanes.filter(_.place == 1).head.hiddenCards.isEmpty)
        assert(game.lanes.filter(_.place == 1).head.visibleCards.size == 1)
        assert(game.lanes.filter(_.place == 2).head.visibleCards.size == 3)
        assert(game.cardsDrawn == 1)
        assert(!game.availableMoves)
      }

      "last in lane to base" in {
        val heartBase = Base(Suite.Heart, List(Card(Rank.Ace, Suite.Heart)))
        val bases = setup.bases.filterNot(_.suite == Suite.Heart) ++ List(heartBase)

        val lane1 = Lane(1, List(), List(card2, card1))
        val lanes = List(lane1) ++ emptyLanes.drop(1)

        val game = Game(List(), bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        solitairePlayer.play(100)

        assert(game.deck.isEmpty)
        assert(game.bases.filter(_.suite == Suite.Heart).head.baseCards.size == 2)
        assert(game.cardsDrawn == 1)
        assert(!game.availableMoves)
      }

      "deck card to base" in {
        val heartBase = Base(Suite.Heart, List(Card(Rank.Ace, Suite.Heart)))
        val bases = setup.bases.filterNot(_.suite == Suite.Heart) ++ List(heartBase)

        val game = Game(List(card1), bases, emptyLanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        solitairePlayer.play(100)

        assert(game.deck.isEmpty)
        assert(game.bases.filter(_.suite == Suite.Heart).head.baseCards.size == 2)
        assert(game.cardsDrawn == 1)
        assert(!game.availableMoves)
      }

      "deck card to lane" in {
        val lane1 = Lane(1, List(), List(card2))
        val lanes = List(lane1) ++ emptyLanes.drop(1)

        val game = Game(List(card1), setup.bases, lanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        solitairePlayer.play(100)

        assert(game.deck.isEmpty)
        assert(game.lanes.filter(_.place == 1).head.visibleCards.size == 2)
        assert(game.cardsDrawn == 1)
        assert(!game.availableMoves)
      }

      "draw card" in {
        val game = Game(List(card1, card2), setup.bases, emptyLanes)
        val solitairePlayer = SolitairePlayer(game, displayer)

        solitairePlayer.play(100)

        assert(game.deck.size == 2)
        assert(game.cardsDrawn == 10)
        assert(!game.availableMoves)
      }

    }

    "complete some games" in {

      assert(
        Range(0, 30).exists(a => {
          val setup = new Setup

          val game = Game(setup.deck, setup.bases, setup.lanes)
          val solitairePlayer = SolitairePlayer(game, displayer)

          solitairePlayer.play(1000)
          solitairePlayer.game.isComplete
        })
      )

    }

  }

}
