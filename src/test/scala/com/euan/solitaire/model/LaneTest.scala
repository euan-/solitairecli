package com.euan.solitaire.model

import org.scalatest.FreeSpec

class LaneTest extends FreeSpec {

  "should" - {

    "display correctly" - {

      "when lane is populated" in {
        val card1 = Card(Rank.Jack, Suite.Spade)
        val card2 = Card(Rank.Ten, Suite.Diamond)
        val card3 = Card(Rank.Nine, Suite.Spade)

        val lane = Lane(1, List(card1, card1), List(card1, card2, card3))

        val rawDisplay = lane.rawDisplay()

        assert(rawDisplay != null)
        assert(rawDisplay.contains(Rank.Jack.display))
        assert(rawDisplay.contains(Rank.Ten.display))
        assert(rawDisplay.contains(Rank.Nine.display))

        assert(rawDisplay.contains(Suite.Spade.display))
        assert(rawDisplay.contains(Suite.Diamond.display))

        print(rawDisplay)
      }

      "when lane has one card" in {
        val card1 = Card(Rank.Jack, Suite.Spade)

        val lane = Lane(1, List(), List(card1))

        val rawDisplay = lane.rawDisplay()

        assert(rawDisplay != null)

        print(rawDisplay)
      }

      "when lane is empty" in {
        val lane = Lane(1, List(), List())

        val rawDisplay = lane.rawDisplay()

        assert(rawDisplay != null)

        print(rawDisplay)
      }

    }

    "move" - {

      val cardA = Card(Rank.Ace, Suite.Spade)

      val cardB = Card(Rank.Eight, Suite.Club)
      val cardC = Card(Rank.Seven, Suite.Heart)
      val card6 = Card(Rank.Six, Suite.Club)
      val card5 = Card(Rank.Five, Suite.Heart)
      val card4 = Card(Rank.Four, Suite.Club)

      "all visible cards" in {
        val lane = Lane(1, List(cardA, cardB, cardC), List(card6, card5, card4))

        var removedCards = lane.removeUpToAndIncludingCard(card6)

        assert(removedCards == List(card6, card5, card4))
        assert(lane.hiddenCards == List(cardA, cardB))
        assert(lane.visibleCards == List(cardC))

        removedCards = lane.removeUpToAndIncludingCard(cardC)

        assert(removedCards == List(cardC))
        assert(lane.hiddenCards == List(cardA))
        assert(lane.visibleCards == List(cardB))

        removedCards = lane.removeUpToAndIncludingCard(cardB)

        assert(removedCards == List(cardB))
        assert(lane.hiddenCards == List())
        assert(lane.visibleCards == List(cardA))

        removedCards = lane.removeUpToAndIncludingCard(cardA)

        assert(removedCards == List(cardA))
        assert(lane.hiddenCards == List())
        assert(lane.visibleCards == List())
      }

      "some visible cards" in {
        val lane = Lane(1, List(cardA), List(cardB, cardC, card6, card5, card4))

        var removedCards = lane.removeUpToAndIncludingCard(card4)

        assert(removedCards == List(card4))
        assert(lane.hiddenCards == List(cardA))
        assert(lane.visibleCards == List(cardB, cardC, card6, card5))

        removedCards = lane.removeUpToAndIncludingCard(cardC)

        assert(removedCards == List(cardC, card6, card5))
        assert(lane.hiddenCards == List(cardA))
        assert(lane.visibleCards == List(cardB))

        removedCards = lane.removeUpToAndIncludingCard(cardB)

        assert(removedCards == List(cardB))
        assert(lane.hiddenCards == List())
        assert(lane.visibleCards == List(cardA))
      }

    }

  }

}
