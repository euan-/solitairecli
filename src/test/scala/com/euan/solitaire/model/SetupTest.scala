package com.euan.solitaire.model

import org.scalatest.FreeSpec

class SetupTest extends FreeSpec {

  "should" - {

    val setup = new Setup

    "create" - {

      "deck correctly" in {
        assert(setup.deck.size == 24)
      }

      "bases correctly" in {
        assert(setup.bases.size == 4)
        assert(setup.bases.forall(_.baseCards.isEmpty))
      }

      "lanes correctly" in {
        assert(setup.lanes.forall(_.visibleCards.size == 1))

        val lane1 = setup.lanes.filter(_.place == 1).head
        assert(lane1.hiddenCards.isEmpty)

        val lane2 = setup.lanes.filter(_.place == 2).head
        assert(lane2.hiddenCards.size == 1)

        val lane3 = setup.lanes.filter(_.place == 3).head
        assert(lane3.hiddenCards.size == 2)

        val lane4 = setup.lanes.filter(_.place == 4).head
        assert(lane4.hiddenCards.size == 3)

        val lane5 = setup.lanes.filter(_.place == 5).head
        assert(lane5.hiddenCards.size == 4)

        val lane6 = setup.lanes.filter(_.place == 6).head
        assert(lane6.hiddenCards.size == 5)

        val lane7 = setup.lanes.filter(_.place == 7).head
        assert(lane7.hiddenCards.size == 6)

        val visibleCards = setup.lanes.map(_.visibleCards.size).sum
        val hiddenCards = setup.lanes.map(_.hiddenCards.size).sum

        assert(visibleCards == 7)
        assert(hiddenCards == 21)
      }

    }

  }

}
