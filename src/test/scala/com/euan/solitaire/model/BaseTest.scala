package com.euan.solitaire.model

import org.scalatest.FreeSpec

class BaseTest extends FreeSpec {

  "should" - {

    "display correctly" - {

      "when base is populated" in {
        val card1 = Card(Rank.Ace, Suite.Spade)
        val card2 = Card(Rank.Two, Suite.Spade)
        val card3 = Card(Rank.Three, Suite.Spade)

        val base = Base(Suite.Spade, List(card1, card2, card3))

        val faceDisplay = base.faceDisplay()

        assert(faceDisplay != null)
        assert(faceDisplay.contains(Suite.Spade.display))
        assert(faceDisplay.contains(Rank.Three.display))
      }

      "when base is empty" in {
        val base = Base(Suite.Spade, List())

        val faceDisplay = base.faceDisplay()

        assert(faceDisplay != null)
        assert(faceDisplay.contains(Suite.Spade.display))
      }
    }

  }

}
