package com.euan.solitaire.model

import org.scalatest.FreeSpec

class CardTest extends FreeSpec {

  "should" - {

    "allow a basePlace" - {

      "when the rank and suite is right" in {
        val clubBase = Base(Suite.Club, List(Card(Rank.Two, Suite.Club)))
        val threeOfClubs = Card(Rank.Three, Suite.Club)

        assert(threeOfClubs.canPlaceCardInBase(clubBase))
      }

      "when the rank is right and base is empty" in {
        val clubBase = Base(Suite.Club, List())
        val aceOfClubs = Card(Rank.Ace, Suite.Club)

        assert(aceOfClubs.canPlaceCardInBase(clubBase))
      }

    }

    "not allow a basePlace" - {

      "when the rank is right but suite is different" in {
        val clubBase = Base(Suite.Club, List(Card(Rank.Two, Suite.Club)))
        val threeOfHearts = Card(Rank.Three, Suite.Heart)

        assert(!threeOfHearts.canPlaceCardInBase(clubBase))
      }

      "when the rank isn't compatible but suite is the same" in {
        val clubBase = Base(Suite.Club, List(Card(Rank.Two, Suite.Club)))
        val fiveOfClubs = Card(Rank.Five, Suite.Club)

        assert(!fiveOfClubs.canPlaceCardInBase(clubBase))
      }

      "when the rank is right but base isn't empty" in {
        val heartBase = Base(Suite.Heart, List(Card(Rank.Two, Suite.Heart)))
        val aceOfClubs = Card(Rank.Ace, Suite.Club)

        assert(!aceOfClubs.canPlaceCardInBase(heartBase))
      }

    }

    "allow a lanePlace" - {

      "when the rank is right and suite colour is different" in {
        val twoOfClubs = Card(Rank.Two, Suite.Club)
        val lane = Lane(1, List(), List(Card(Rank.Three, Suite.Heart)))

        assert(twoOfClubs.canPlaceCardInLane(lane))
      }

      "when the rank is right and lane is empty" in {
        val kingOfClubs = Card(Rank.King, Suite.Club)

        assert(kingOfClubs.canPlaceCardInLane(Lane(1, List(), List())))
      }

    }

    "not allow a lanePlace" - {

      "when the rank is right but suite colour is the same" in {
        val twoOfClubs = Card(Rank.Two, Suite.Club)
        val lane = Lane(1, List(), List(Card(Rank.Three, Suite.Spade)))

        assert(!twoOfClubs.canPlaceCardInLane(lane))
      }

      "when the rank isn't compatible but suite colour is different" in {
        val twoOfClubs = Card(Rank.Two, Suite.Club)
        val lane = Lane(1, List(), List(Card(Rank.Five, Suite.Heart)))

        assert(!twoOfClubs.canPlaceCardInLane(lane))
      }

      "when the rank is right but lane isn't empty" in {
        val kingOfClubs = Card(Rank.King, Suite.Club)
        val lane = Lane(1, List(), List(Card(Rank.Five, Suite.Heart)))

        assert(!kingOfClubs.canPlaceCardInLane(lane))
      }

    }

  }

}
