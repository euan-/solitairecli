package com.euan.solitaire.displayer

import com.euan.solitaire.model.{Game, Setup, Suite}
import org.scalatest.FreeSpec

class DisplayerTest extends FreeSpec {

  "should" - {

    val setup = new Setup
    val game = Game(setup.deck, setup.bases, setup.lanes)

    "display" - {

      "for a new game" - {

        "the base and deck correctly" in {
          val displayer = new Displayer()

          val display = displayer.displayBaseAndDeck(game)

          assert(display != null)
          assert(display.contains(Suite.Diamond.display))
          assert(display.contains(Suite.Spade.display))
          assert(display.contains(Suite.Club.display))
          assert(display.contains(Suite.Heart.display))

          print(display)
        }

        "the lane correctly" in {
          val displayer = new Displayer()

          val display = displayer.displayLanes(game)

          assert(display != null)

          print(display)
        }

        "the full game correctly" in {
          val displayer = new Displayer()

          val display = displayer.display(game)

          assert(display != null)

          print(display)
        }

      }
    }

  }

}
