package com.euan.solitaire.displayer

import com.euan.solitaire.model.{Game, Suite}

class Displayer() {

  val rowBar: String = "___________________________________________"

  def display(game: Game): String = {
    displayBaseAndDeck(game) + "\n" + displayLanes(game)
  }

  def displayBaseAndDeck(game: Game): String = {
    val dh = game.deck.headOption
    val ds = if (dh.isDefined) dh.get.display else "   "

    val bh = game.bases.filter(_.suite == Suite.Heart).head.faceDisplay()
    val bd = game.bases.filter(_.suite == Suite.Diamond).head.faceDisplay()
    val bs = game.bases.filter(_.suite == Suite.Spade).head.faceDisplay()
    val bc = game.bases.filter(_.suite == Suite.Club).head.faceDisplay()

    s"""
      | $rowBar
      ||  ___   ___   ___   ___               ___  |
      || |$bh| |$bd| |$bs| |$bc|             |$ds| |
      || |   | |   | |   | |   |             |   | |
      || |___| |___| |___| |___|             |___| |
      ||                                           |
      ||   1     2     3     4     5     6     7   |""".stripMargin
  }

  def displayLanes(game: Game): String = {
    val maxRows = game.lanes.sortBy(_.place).map(_.display().size).max

    Range(0, maxRows).map(rowNum => "| " + getRowString(game, rowNum) + "|" + "\n").mkString + "|" + rowBar + "|"
  }

  private def getRowString(game: Game, rowNum: Int): String = {
    game.lanes.sortBy(_.place).map(lane => {
      val rows = lane.display()
      if (rows.size > rowNum) rows(rowNum) + " " else "      "
    }).mkString
  }

}