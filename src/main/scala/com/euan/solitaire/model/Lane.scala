package com.euan.solitaire.model

case class Lane(place: Int, var hiddenCards: List[Card], var visibleCards: List[Card]) {

  private val roof: String = " ___ "
  private def top(filler: String = "   "): String = "|" + filler + "|"
  private val empty: String = top()
  private val bottom: String = top("___")

  def addCardsToVisibleCards(cardsToAdd: List[Card]): Unit = {
    visibleCards = visibleCards ++ cardsToAdd
  }

  def removeUpToAndIncludingCard(card: Card): List[Card] = {
    val indexOfCard = visibleCards.indexOf(card)

    val (cardsRemaining, cardsToMove) = visibleCards.splitAt(indexOfCard)

    visibleCards = cardsRemaining

    if (visibleCards.isEmpty)
      moveTopHiddenCardToTopOfVisible()

    cardsToMove
  }

  private def moveTopHiddenCardToTopOfVisible(): Unit = {
    if (lastHiddenCard().nonEmpty) {
      visibleCards = List(lastHiddenCard().get)
      hiddenCards = hiddenCards.dropRight(1)
    }
  }

  def lastHiddenCard(): Option[Card] = {
    hiddenCards.lastOption
  }

  def topVisibleCard(): Option[Card] = {
    visibleCards.headOption
  }

  def lastVisibleCard(): Option[Card] = {
    visibleCards.lastOption
  }

  def display(): List[String] = {
    rawDisplay().split("\n").toList
  }

  def rawDisplay(): String = {
    if (hiddenCards.isEmpty && visibleCards.isEmpty) {
      Seq(roof, top(), empty, bottom).mkString("\n")
    } else if (hiddenCards.nonEmpty && visibleCards.nonEmpty) {
      hiddenCardsDisplay() + "\n" + visibleCardsDisplay()
    } else {
      if (visibleCards.size > 1) roof + "\n" + visibleCardsDisplay() else visibleCardsDisplay()
    }
  }

  private def hiddenCardsDisplay(): String = {
    var output = roof

    if (hiddenCards.nonEmpty) {
      output = (Seq(output) ++ hiddenCards.indices.map(a => bottom)).mkString("\n")
    }

    output
  }

  private def visibleCardsDisplay(): String = {
    var output = ""

    if (visibleCards.nonEmpty) {
      val last = visibleCards.takeRight(1)

      val notLastCards = visibleCards.dropRight(1).map(card => top(card.display))

      val finalCard = Seq(roof, top(last.head.display), empty, bottom)

      output = (notLastCards ++ finalCard).mkString("\n")
    }

    output
  }

}
