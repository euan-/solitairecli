package com.euan.solitaire.model

object Constants {

  val helpOptions: String =
    Seq(
      "Deck to Base :- " + deckToBaseExample,
      "Deck to Lane 1 :- " + deckToLaneOneExample,
      "Spade Base to Lane 2 :- " + heartBaseToLaneOneExample,
      "Lane 2, 3rd card down to Lane 3 :- " + laneTwoOneCardDownToLaneOneExample,
      "Lane 1, 2nd card down to base :- " + laneOneSecondCardDownToBaseExample,
      "Draw a card :- " + drawACardExample,
      "List of available moves :- " + availableMovesExample,
      "Ask for help :- " + askForHelpExample,
      "End Game :- " + endGameExample
    ).mkString("\n")

  val wrongMove: String = "Error in processing your move - Please try again."

  val unacceptableMove: String = "Invalid move - Please try another"

  val endingGame: String = "Ending Game - Thanks for playing!"

  val drawingACard: String = "Drawing a Card"

  val deckIndicator: Char = 'd'

  val laneIndicator: Char = 'l'

  val baseIndicator: Char = 'b'

  val space: Char = ' '

  def deckToBaseExample: String = "\"d b\""

  def deckToLaneOneExample: String = "\"d 1\""

  def heartBaseToLaneOneExample: String = "\"hb 1\""

  def laneTwoOneCardDownToLaneOneExample: String = "\"2l1 1\""

  def laneOneSecondCardDownToBaseExample: String = "\"1l2 b\""

  def drawACardExample: String = "\"d\""

  def availableMovesExample: String = "\"o\""

  def askForHelpExample: String = "\"h\""

  def endGameExample: String = "\"e\""

}
