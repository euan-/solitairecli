package com.euan.solitaire.model

import com.euan.solitaire.model.Suite.Suite

case class Base(suite: Suite, var baseCards: List[Card]) {

  def addCardToBase(card: Card): Unit = {
    baseCards = baseCards ++ List(card)
  }

  def removeCardFromBase(): Card = {
    val card = lastVisibleCard().get
    baseCards = baseCards.dropRight(1)
    card
  }

  def faceDisplay(): String = {
    if (baseCards.nonEmpty) baseCards.last.display else "  " + suite.display
  }

  def lastVisibleCard(): Option[Card] = {
    baseCards.lastOption
  }

}
