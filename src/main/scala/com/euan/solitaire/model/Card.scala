package com.euan.solitaire.model

import com.euan.solitaire.model.Colour.Colour
import com.euan.solitaire.model.Rank.Rank
import com.euan.solitaire.model.Suite.Suite

case class Card(rank: Rank, suite: Suite) {

  def canPlaceCardInBase(base: Base): Boolean = {
    (base.baseCards.isEmpty && base.suite == suite && rank == Rank.Ace) ||
      (base.baseCards.nonEmpty && base.lastVisibleCard().get.suite == suite && rank.id - base.lastVisibleCard().get.rank.id == 1)
  }

  def canPlaceCardInLane(lane: Lane): Boolean = {
    (lane.visibleCards.isEmpty && rank == Rank.King) ||
      (lane.visibleCards.nonEmpty && lane.lastVisibleCard().get.suite.colour != suite.colour &&
        lane.lastVisibleCard().get.rank.id - rank.id == 1)
  }

  def display: String = rank.display + suite.display

}

object Rank extends Enumeration {
  type Rank = Value

  val Ace = Val(" A")
  val Two = Val(" 2")
  val Three = Val(" 3")
  val Four = Val(" 4")
  val Five = Val(" 5")
  val Six = Val(" 6")
  val Seven = Val(" 7")
  val Eight = Val(" 8")
  val Nine = Val(" 9")
  val Ten = Val("10")
  val Jack = Val(" J")
  val Queen = Val(" Q")
  val King = Val(" K")

  protected case class Val(display: String) extends super.Val

  implicit def valueToRankVal(x: Value): Val = x.asInstanceOf[Val]
}

object Suite extends Enumeration {
  type Suite = Value

  protected case class Val(colour: Colour, display: String) extends super.Val

  implicit def valueToSuiteVal(x: Value): Val = x.asInstanceOf[Val]

  val Heart = Val(Colour.Red, "♥")
  val Diamond = Val(Colour.Red, "♦")
  val Spade = Val(Colour.Black, "♠")
  val Club = Val(Colour.Black, "♣")
}

object Colour extends Enumeration {
  type Colour = Value
  val Red, Black = Value
}