package com.euan.solitaire.model

import scala.collection.mutable.ListBuffer
import scala.util.Random

class Setup {

  private val deckLb: ListBuffer[Card] = {
    var lb: ListBuffer[Card] = ListBuffer()

    val allCards =
      Suite.values.flatMap(suite => Rank.values.map(rank => Card(rank, suite))).toList

    lb.insertAll(0, Random.shuffle(allCards))

    lb
  }

  val lanes: List[Lane] = {
    List(
      Lane(1, List(), headCardFromDeck()),
      Lane(2, removeNCardsFromDeck(1), headCardFromDeck()),
      Lane(3, removeNCardsFromDeck(2), headCardFromDeck()),
      Lane(4, removeNCardsFromDeck(3), headCardFromDeck()),
      Lane(5, removeNCardsFromDeck(4), headCardFromDeck()),
      Lane(6, removeNCardsFromDeck(5), headCardFromDeck()),
      Lane(7, removeNCardsFromDeck(6), headCardFromDeck()),
    )
  }

  val bases: List[Base] = {
    List(
      Base(Suite.Heart, List()),
      Base(Suite.Diamond, List()),
      Base(Suite.Spade, List()),
      Base(Suite.Club, List())
    )
  }

  val deck: List[Card] = deckLb.toList

  private def removeNCardsFromDeck(n: Int): List[Card] = {
    Range(0 , n).map(index => deckLb.remove(index)).toList
  }

  private def headCardFromDeck(): List[Card] = {
    List(deckLb.remove(0))
  }

}