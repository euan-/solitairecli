package com.euan.solitaire.model

case class Game(var deck: List[Card], var bases: List[Base], var lanes: List[Lane]) {

  var cardsDrawn = 0

  def isComplete: Boolean =
    bases.flatMap(_.baseCards).size == 52

  var availableMoves: Boolean = true

  def drawCard(): Unit = {
    cardsDrawn += 1

    if (deck.nonEmpty) {
      val topCard = deck.head
      deck = deck.drop(1)
      deck = deck ++ Seq(topCard)
    }

    if (cardsDrawn >= 5 * deck.size) {
      availableMoves = false
    }
  }

}
