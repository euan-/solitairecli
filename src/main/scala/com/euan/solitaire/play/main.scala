package com.euan.solitaire.play

import com.euan.solitaire.ai.SolitairePlayer
import com.euan.solitaire.displayer.Displayer
import com.euan.solitaire.model.{Game, Setup}

import scala.io.StdIn

object main extends App {

  val setup = new Setup
  val displayer = new Displayer()
  val game = Game(setup.deck, setup.bases, setup.lanes)
  val solitairePlayer = SolitairePlayer(game, displayer)
  val inputParser: InputParser = new InputParser

  print("\nWelcome to CLI Solitaire!")

  println(displayer.display(game))

  println("\nEnter your first move or type \"o\" for the acceptable move guide\n")

  while (!game.isComplete && game.availableMoves) {
    val input = StdIn.readLine()
    val moveMade = inputParser.parseInputAndMakeMove(input.trim, game, solitairePlayer)
    println(moveMade)
    println(displayer.display(game))
  }

}
