package com.euan.solitaire.play

import com.euan.solitaire.ai.SolitairePlayer
import com.euan.solitaire.model._

class InputParser {

  def parseInputAndMakeMove(input: String, game: Game, solitairePlayer: SolitairePlayer): String = {
    try {
      input match {
        case _ if oneCountOf(Constants.deckIndicator, input) && oneCountOf(Constants.baseIndicator, input) && oneCountOf(Constants.space, input) =>
          deckToBase(input, game)
        case _ if oneCountOf(Constants.deckIndicator, input) && oneCountOf(Constants.space, input) =>
          deckToLane(input, game)
        case _ if oneCountOf(Constants.laneIndicator, input) && !input.contains(Constants.baseIndicator) && oneCountOf(Constants.space, input) =>
          laneToLane(input, game)
        case _ if !input.contains(Constants.laneIndicator) && oneCountOf(Constants.baseIndicator, input) && oneCountOf(Constants.space, input) =>
          baseToLane(input, game)
        case _ if oneCountOf(Constants.laneIndicator, input) && oneCountOf(Constants.baseIndicator, input) && oneCountOf(Constants.space, input) =>
          laneToBase(input, game)
        case "d" =>
          game.drawCard()
          Constants.drawingACard
        case "o" => Constants.helpOptions
        case "h" => solitairePlayer.recommendMove()
        case "e" =>
          game.availableMoves = false
          Constants.endingGame
        case _ => Constants.unacceptableMove
      }
    } catch {
      case _: Exception => Constants.wrongMove
    }
  }

  private def deckToLane(input: String, game: Game): String = {
    val cards: Array[String] = input.split(Constants.space)

    val card1: Card = game.deck.head

    val laneTo: Lane = game.lanes(cards(1).toInt - 1)

    if (card1.canPlaceCardInLane(laneTo)) {
      laneTo.addCardsToVisibleCards(List(card1))
      game.deck = game.deck.drop(1)

      s"Moved ${card1.display} to lane ${laneTo.place}"
    } else Constants.unacceptableMove
  }

  private def deckToBase(input: String, game: Game): String = {
    val cards: Array[String] = input.split(Constants.space)

    val card1: Card = game.deck.head

    val base: Base = game.bases.filter(_.suite == card1.suite).head

    if (card1.canPlaceCardInBase(base)) {
      base.addCardToBase(card1)
      game.deck = game.deck.drop(1)

      s"Moved ${card1.display} to base"
    } else Constants.unacceptableMove
  }

  private def laneToLane(input: String, game: Game): String = {
    val cards: Array[String] = input.split(Constants.space)

    val (laneFrom, card1): (Lane, Card) = {
      val numbers = cards(0).split(Constants.laneIndicator)
      val lane = game.lanes(numbers(0).toInt - 1)
      (lane, lane.visibleCards(numbers(1).toInt - 1))
    }

    val laneTo: Lane = game.lanes(cards(1).toInt - 1)

    if (card1.canPlaceCardInLane(laneTo)) {
      val removedCards = laneFrom.removeUpToAndIncludingCard(card1)

      laneTo.addCardsToVisibleCards(removedCards)

      s"Moved ${card1.display} to lane ${laneTo.place}"
    } else Constants.unacceptableMove
  }

  private def laneToBase(input: String, game: Game): String = {
    val cards: Array[String] = input.split(Constants.space)

    val (laneFrom, card1): (Lane, Card) = {
      val numbers = cards(0).split(Constants.laneIndicator)
      val lane = game.lanes(numbers(0).toInt - 1)
      (lane, lane.visibleCards(numbers(1).toInt - 1))
    }

    val base: Base = game.bases.filter(_.suite == card1.suite).head

    if (card1.canPlaceCardInBase(base) && laneFrom.lastVisibleCard().get == card1) {
      val removedCard = laneFrom.removeUpToAndIncludingCard(card1).head

      base.addCardToBase(removedCard)

      s"Moved ${card1.display} to base"
    } else Constants.unacceptableMove
  }

  private def baseToLane(input: String, game: Game): String = {
    val cards: Array[String] = input.split(Constants.space)

    val base: Base = {
      val numbers = cards(0).split(Constants.baseIndicator)
      game.bases.filter(_.suite.toString.charAt(0).toString.equalsIgnoreCase(numbers(0))).head
    }

    val laneTo: Lane = game.lanes(cards(1).toInt - 1)

    if (base.lastVisibleCard().get.canPlaceCardInLane(laneTo)) {
      val removedCard = base.removeCardFromBase()

      laneTo.addCardsToVisibleCards(List(removedCard))

      s"Moved ${removedCard.display} to lane ${laneTo.place}"
    } else Constants.unacceptableMove
  }

  private def oneCountOf(character: Char, input: String): Boolean = {
    input.contains(character) && input.count(_ == character) == 1
  }

}