package com.euan.solitaire.ai

import com.euan.solitaire.displayer.Displayer
import com.euan.solitaire.model.{Game, Lane, Rank}

case class SolitairePlayer(game: Game, displayer: Displayer) {

  def recommendMove(): String = {
    tryToCarryOutMoveInGame()
  }

  def play(numberOfMovesAllowed: Int, printBoard: Boolean = false): Unit = {
    var movesMade = 0
    var moveMade = ""

    if (printBoard) {
      print(displayer.display(game))
      println
      println
    }

    while (!game.isComplete && game.availableMoves && movesMade < numberOfMovesAllowed) {
      movesMade += 1

      moveMade = tryToCarryOutMoveInGame(true)

      if (printBoard) {
        println(s"------------------ Move No: $movesMade ------------------")
        print(s"Move: $moveMade")

        print(displayer.display(game))
        println
        println
      }
    }

    println(s"Game has ended after $movesMade moves ${if (!game.availableMoves) "as no more were possible"} - Game was ${if (game.isComplete) "Completed" else "Unfinished"}")
  }

  private def tryToCarryOutMoveInGame(carryOutMove: Boolean = false): String = {
    var moveCarriedOut: String = ""

    if (moveCarriedOut.isEmpty) moveCarriedOut = lastInLaneToBase(carryOutMove)
    if (moveCarriedOut.isEmpty) moveCarriedOut = deckCardToBase(carryOutMove)
    if (moveCarriedOut.isEmpty) moveCarriedOut = firstInLaneToLaneRevealingHiddenCard(carryOutMove)
    if (moveCarriedOut.isEmpty) moveCarriedOut = deckCardToLane(carryOutMove)
    if (moveCarriedOut.isEmpty) moveCarriedOut = drawCard(carryOutMove)

    moveCarriedOut
  }

  private def firstInLaneToLaneRevealingHiddenCard(carryOutMove: Boolean): String = {
    val laneMove: Option[Lane] = game.lanes
      .find(lane => lane.topVisibleCard().isDefined && !(lane.topVisibleCard().get.rank == Rank.King && lane.hiddenCards.isEmpty) &&
        game.lanes.exists(otherLane => otherLane.place != lane.place &&
          lane.topVisibleCard().get.canPlaceCardInLane(otherLane)))

    if (laneMove.isDefined) {
      game.cardsDrawn = 0

      val movingLane = laneMove.get
      val laneToMoveTo = game.lanes.filter(otherLane => otherLane.place != movingLane.place &&
        movingLane.topVisibleCard().get.canPlaceCardInLane(otherLane)).head

      val move = s"Moving ${movingLane.topVisibleCard().get.display} from lane ${movingLane.place} to lane ${laneToMoveTo.place}"

      if (carryOutMove) {
        val removedCards = movingLane.removeUpToAndIncludingCard(movingLane.topVisibleCard().get)

        laneToMoveTo.addCardsToVisibleCards(removedCards)
      }

      move
    } else ""
  }

  private def lastInLaneToBase(carryOutMove: Boolean): String = {
    val laneToBase: Option[Lane] = game.lanes
      .find(lane => lane.lastVisibleCard().isDefined &&
        game.bases.exists(b => lane.lastVisibleCard().get.canPlaceCardInBase(b)))

    if (laneToBase.isDefined) {
      game.cardsDrawn = 0

      val lane = laneToBase.get
      val baseToMoveTo = game.bases.filter(base => lane.lastVisibleCard().get.canPlaceCardInBase(base)).head

      val move = s"Moving ${lane.lastVisibleCard().get.display} from ${lane.place} to ${baseToMoveTo.suite.display} base"

      if (carryOutMove) {
        val cardToBase = lane.removeUpToAndIncludingCard(lane.lastVisibleCard().get).head
        baseToMoveTo.addCardToBase(cardToBase)
      }

      move
    } else ""
  }

  private def deckCardToBase(carryOutMove: Boolean): String = {
    if (game.deck.nonEmpty && game.bases.exists(b => game.deck.head.canPlaceCardInBase(b))) {
      game.cardsDrawn = 0

      val baseToMoveTo = game.bases.filter(game.deck.head.canPlaceCardInBase(_)).head

      val move = s"Moving ${game.deck.head.display} from deck to ${baseToMoveTo.suite.display} base}"

      baseToMoveTo.addCardToBase(game.deck.head)
      game.deck = game.deck.drop(1)

      move
    } else ""
  }

  private def deckCardToLane(carryOutMove: Boolean): String = {
    if (game.deck.nonEmpty && game.lanes.exists(game.deck.head.canPlaceCardInLane(_))) {
      game.cardsDrawn = 0

      val laneToMoveTo = game.lanes.filter(game.deck.head.canPlaceCardInLane(_)).head

      val move = s"Moving ${game.deck.head.display} from deck to lane ${laneToMoveTo.place}"

      if (carryOutMove) {
        laneToMoveTo.addCardsToVisibleCards(List(game.deck.head))
        game.deck = game.deck.drop(1)
      }

      move
    } else ""
  }

  private def drawCard(carryOutMove: Boolean): String = {
    if (carryOutMove) {
      game.drawCard()

      s"Drawing a card from the deck (${game.cardsDrawn} cards drawn in a row)"
    } else "Draw a card"
  }

}
