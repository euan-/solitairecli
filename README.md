# Scala Solitaire CLI

Simple Solitaire program, written in Scala, with helper included

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for playing.

### Installing

Clone the repo and switch to master branch on your local machine

```
git clone https://USERNAME@bitbucket.org/euan-/solitairecli.git
```

Import as a pom project in IntelliJ, reimport and run a clean install

### Playing a game

* Run **main** class under **play** module
* Type "o" to see a move list
* Type your move and hit enter to play

## Running the tests

Simply run a clean install on the root pom to run all tests

```
mvn clean install
```

## Built With

* [Scala 2.12.8](https://www.scala-lang.org/news/2.12.8) - Development language
* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Euan Maciver** - *Initial build* - 20190217 - [euan-](https://bitbucket.org/euan-/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details